import csv
import os
import secrets
import shlex
import string
import subprocess
from pydub.utils import mediainfo

import boto3 as boto3

tmp_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'tmp')
raw_audio_dir = os.path.join(tmp_dir, 'audio', 'raw')
processed_audio_dir = os.path.join(tmp_dir, 'audio', 'processed')
raw_video_dir = os.path.join(tmp_dir, 'video', 'raw')
processed_video_dir = os.path.join(tmp_dir, 'video', 'processed')

input_csv_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'csv', 'input.csv')
output_csv_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'csv', 'output.csv')

processed_audio_s3_dir = "audio/mandir/songs"
processed_video_s3_dir = "videos/mandir/songPreview/processed"

raw_bucket = "a4b-general-backups-internal"
processed_bucket = "cdn.appsforbharat.com"


def random_string(size):
    letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    return ''.join(secrets.choice(letters) for i in range(size))


def read_csv(file_path):
    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        audios = {}
        for row in csv_reader:
            try:
                audios[row['audio_id']] = row
            except Exception as e:
                print('exception happened:' + str(e))
                continue
    return audios


def write_csv(data, file_path):
    with open(file_path, 'w') as f:
        write = csv.writer(f)
        write.writerows(data)
    return True


def download_s3(bucket, key, file_path):
    s3_resource = boto3.resource('s3')
    s3_resource.Object(bucket, key).download_file(file_path)


def upload_s3(bucket, key, file_path):
    s3_resource = boto3.resource('s3')
    s3_resource.meta.client.upload_file(file_path, bucket, key)


def process_video(audio_file_path, raw_video_file_path, start_time):
    processed_video_file_path = os.path.join(processed_video_dir, f'{random_string(20)}.mp4')

    command = f'ffmpeg ' \
              f'-ss 00:00:00  -t 28 -i {raw_video_file_path} ' \
              f'-ss {start_time} -t 28 -i {audio_file_path} ' \
              f'-map 0:v ' \
              f'-map 1:a ' \
              f'-c:v libx265 -crf 28 ' \
              f'-vf \'scale=trunc(iw/2):trunc(ih/2)\' ' \
              f'-y {processed_video_file_path} '

    cmd = subprocess.run(shlex.split(command), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return processed_video_file_path


def process_audio(raw_audio_file_path):
    processed_audio_file_path = os.path.join(processed_audio_dir, f'{random_string(20)}.mp3')
    command = f"ffmpeg -i {raw_audio_file_path} -ab 192k {processed_audio_file_path}"
    cmd = subprocess.run(shlex.split(command), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    return processed_audio_file_path


def get_audio_info(audio_file_path):
    info = mediainfo(audio_file_path)
    return {"duration": int(float(info['duration'])), "bitrate": int(float(info['bit_rate']) / 1024)}


if __name__ == '__main__':
    get_video = False
    audios = read_csv(input_csv_path)
    result = []
    report = []
    for audio_id, audio_info in audios.items():
        try:
            # get raw audio
            raw_audio_path = audio_info['raw_audio_path']
            raw_audio_file_path = os.path.join(raw_audio_dir, raw_audio_path.split("/")[-1])
            download_s3(raw_bucket, raw_audio_path, raw_audio_file_path)

            # process raw audio
            processed_audio_file_path = process_audio(raw_audio_file_path)
            processed_audio_file_name = processed_audio_file_path.split("/")[-1]

            # get audio info
            processed_audio_info = get_audio_info(processed_audio_file_path)

            # upload audio
            upload_s3(processed_bucket, f'{processed_audio_s3_dir}/{processed_audio_file_name}', processed_audio_file_path)

            if get_video:
                # process video
                god_id = int(audio_info['god_id'])
                if god_id not in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18]:
                    raw_video_file_path = os.path.join(raw_video_dir, 'god_generic.mp4')
                else:
                    raw_video_file_path = os.path.join(raw_video_dir, f'god_{god_id}.mp4')
                processed_video_file_path = process_video(processed_audio_file_path, raw_video_file_path,
                                                          audio_info['start_time'])
                processed_video_file_name = processed_video_file_path.split("/")[-1]

                # upload video
                upload_s3(processed_bucket, f'{processed_video_s3_dir}/{processed_video_file_name}', processed_video_file_path)
            else:
                processed_video_file_name = ""

            result = [
                True,
                audio_id,
                f'https://cdn.appsforbharat.com/{processed_audio_s3_dir}/{processed_audio_file_name}',
                f'https://cdn.appsforbharat.com/{processed_video_s3_dir}/{processed_video_file_name}',
                processed_audio_info['bitrate'],
                processed_audio_info['duration']
            ]
        except Exception as e:
            print(e)
            result = [False, audio_id, '', '', '', '']
        print(result)
        report.append(result)

    write_csv(report, output_csv_path)
